//
//  SimpleTasksVC.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/22/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class SimpleTasksVC: UIViewController {

    @IBOutlet weak var messagelabel: UILabel!
    
    @IBOutlet weak var emptyImageView: UIImageView!
    @IBOutlet weak var addNewTaskLabel: UILabel!
    
    @IBOutlet weak var addTaskStackView: UIStackView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        setupMessageLabel()

    }

    private func setupMessageLabel() {
         messagelabel.text = "There's nothing here yet."
         addNewTaskLabel.text = "Add new task..."
    }
    
    fileprivate func setupNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        title = "All Tasks"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
