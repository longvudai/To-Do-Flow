//
//  TasksVC.swift
//  TO-DO Flow
//
//  Created by Dai Long on 7/17/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import CoreData
import EventKit

class TasksVC: SimpleTasksVC {

    @IBOutlet weak var popupView: PopupView!
    
    @IBOutlet weak var bottomPopupView: NSLayoutConstraint!
    @IBOutlet weak var heightPopupView: NSLayoutConstraint!
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var inputTF: UITextField!
    
    // MARK: - Properties
    
    var viewModel: TaskViewViewModel? {
        didSet {
            viewModel?.fetchedResultsController.delegate = self
        }
    }
    
    // MARK: -

    private var tags: [Tag]?

    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        updateView()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification(notification:)),
                                               name: Notification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    
    deinit {
        print("TasksVC Was Deinited")
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
             //   self.testView.isHidden = true
                self.keyboardHeightLayoutConstraint?.constant = 0
            } else {
             //   self.testView.isHidden = false
                self.keyboardHeightLayoutConstraint?.constant = (endFrame?.size.height)! - (tabBarController?.tabBar.frame.height)!
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let identifier = segue.identifier else {return}
//
//        switch identifier {
//        case Segue.addTaskVC:
//            guard let destionation = segue.destination as? AddTaskVC else {return}
//
//            // Configure Context
//            destionation.managedObjectContext = managedObjectContext
//
//        default:
//            break
//        }
//    }
//
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        switch identifier {
//        case Segue.addTaskVC:
//            if let taskTitle = inputTF.text, !taskTitle.isEmpty {
//                // Create Task
//                let task = Task(context: managedObjectContext!)
//
//                // Configure Task
//                task.updatedAt = Date()
//                task.createdAt = Date()
//                task.title = taskTitle
//
//                clearTextField(inputTF)
//                dismissKeyboard()
//
//                return false
//            }
//            else {
//                return true
//            }
//        default:
//            return false
//        }
//    }

    // MARK: - Methods
    private func setupView() {
        
        setupTableView()
        view.backgroundColor = .white
        inputTF.delegate = self
        setupPopupView()
        
        self.hideKeyboardWhenTappedAround()
    }

    
    private func updateView() {
        guard let viewModel = self.viewModel else { return }
            tableView.isHidden = !viewModel.hasTasks
            messagelabel.isHidden = viewModel.hasTasks
            addNewTaskLabel.isHidden = viewModel.hasTasks
            emptyImageView.isHidden = viewModel.hasTasks
    }

    
    private func setupPopupView() {
        
        popupView.backgroundColor = .clear
        popupView.isOpaque = false
        popupView.tableView.backgroundView = nil
        popupView.tableView.backgroundColor = .clear
    //    popupView.delegate = self
        heightPopupView.constant = 0
    }
    

    // MARK: - Actions
    
    // Sort Button
    @IBAction func sortWasPressed(_ sender: UIBarButtonItem) {
    //    configure()
    //    animateIn()
    //    addTaskStackView.isHidden = true
    }
}


/* -------------------------- */
extension TasksVC: UITableViewDataSource, UITableViewDelegate {
    fileprivate func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.register(TaskCell.nib, forCellReuseIdentifier: TaskCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = self.viewModel else { return 0 }
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = self.viewModel else { return nil }
        return viewModel.titleForSection(for: section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = self.viewModel else { return 0 }
        return viewModel.numberOfRows(for: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Dequeue Reusable Cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TaskCell.identifier, for: indexPath) as? TaskCell else {
            fatalError("Unexpected Index Path")
        }
        
        guard let viewModel = self.viewModel else { fatalError("View Model is nil") }
        
        let taskViewDataViewModel = viewModel.viewModel(for: indexPath)

        // Configure Cell
        cell.configure(withViewModel: taskViewDataViewModel)
        cell.taskTitleTextView.delegate = self
        cell.delegate = self
        cell.setupSwipe()
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

//        // Configure Cell

//        if tags?.count == 0 {
//            cell.heightTaskCellConstraint.priority = UILayoutPriority(rawValue: 999)
//            cell.heightTaskTagExpanded.priority = UILayoutPriority(rawValue: 250)
//        }
//        else { cell.heightTaskCellConstraint.priority = UILayoutPriority(rawValue: 250)
//            cell.heightTaskTagExpanded.priority = UILayoutPriority(rawValue: 999)
//        }
//    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        // Fetch Task
        guard let viewModel = self.viewModel else {return}
        
        let task = viewModel.fetchedResultsController.object(at: indexPath)
        
        // Delete Note
        task.managedObjectContext?.delete(task)
    }
}

/* -----------------------------------*/
extension TasksVC: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        guard let indexPath = tableView.indexPath(for: textView) else { return false }
        let cell = tableView.cellForRow(at: indexPath) as! TaskCell
        
        cell.infoButton.isHidden = false
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        guard let indexPath = tableView.indexPath(for: textView) else { return false }
        let cell = tableView.cellForRow(at: indexPath) as! TaskCell
        
        cell.infoButton.isHidden = true
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        tableView.beginUpdates()
        fixTextView(textView)
        tableView.endUpdates()
    }
}

extension TasksVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let taskTitle = textField.text, !taskTitle.isEmpty {
            // Create Task
            if let viewModel = self.viewModel {
                let task = Task(context: viewModel.fetchedResultsController.managedObjectContext)
                
                // Configure Task
                task.updatedAt = Date()
                task.createdAt = Date()
                task.title = taskTitle
            }
            
        }
        
        clearTextField(inputTF)
        dismissKeyboard()
        return true
    }

    private func clearTextField(_ textField: UITextField) {
        textField.text = ""
    }
}


extension TasksVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        updateView()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? TaskCell {
                let taskViewDataViewModel = viewModel!.viewModel(for: indexPath)
                cell.configure(withViewModel: taskViewDataViewModel)
            }
        case .move:

            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }

            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        }
    }

}

extension TasksVC: TaskCellDelegate {

    func swipwRight(_ taskCell: TaskCell) {
        guard let indexPath = tableView.indexPath(for: taskCell) else { return }

        guard let viewModel = self.viewModel else { return }
        
        // fetch task
        let task = viewModel.fetchedResultsController.object(at: indexPath)
        
        task.isFinished = !task.isFinished

        task.hasAlarm = false
     //   task.calendarIdentifier = nil
    }
    
    func showDetail(_ sender: UIButton) {
        guard let detailsNavigationVC = storyboard?.instantiateViewController(withIdentifier: "DetailsNavigationVC") as? UINavigationController else { return }

        let detailsTableVC = detailsNavigationVC.topViewController as! DetailsTableVC
        guard let indexPath = tableView.indexPath(for: sender) else {return}
        
        guard let viewModel = self.viewModel else { return }
        
        // fetch task
        let task = viewModel.fetchedResultsController.object(at: indexPath)

        // Configure Destination
        
        
        
        detailsTableVC.viewModels = [
            TaskTitleViewModel(task: task),
            RemindOnDateViewModel(task: task),
            RemindAtLocationViewModel(task: task),
            CategoriesAndTagsViewModel(task: task),
            NotesViewModel(task: task)
        ]
        present(detailsNavigationVC, animated: true, completion: nil)
    }
}
//
//
//extension TasksVC: PopupViewDelegate {
//    func sortByCategory() {
//
//    }
//
//    func configure() {
//        self.updateViewConstraints()
//    }
//
//    func animateIn() {
//
//        configure()
//        popupView.alpha = 0.8
//
//
//        UIView.animate(withDuration: 2,
//                       delay: 0,
//                       options: [.curveEaseIn],
//                       animations: {
//                        self.popupView.alpha = 1
//                        self.heightPopupView.constant = 310
//                        self.bottomPopupView.constant = 20
//                        self.view.layoutIfNeeded()
//                        print("ád")
//        })
//    }
//
//    func animateOut () {
//        self.bottomPopupView.constant = -self.popupView.frame.height
//
//        UIView.animate(withDuration: 2,
//                       delay: 0,
//                       options: [.curveEaseOut],
//                       animations: {
//                    //    self.popupView.alpha = 1
//                    //    self.bottomPopupView.constant = -self.popupView.frame.height
//                      //  self.view.layoutIfNeeded()
//                        self.view.setNeedsLayout()
//                        print("hêl")
//        })
//        { (success: Bool) in
//            print("remove")
//          //  self.tableView.reloadData()
//            self.addTaskStackView.isHidden = false
//        }
//    }
//
//    func dismissView() {
//        animateOut()
//    }
//
//    func sortByGroceryList() {
//        animateOut()
//        self.sortByDay = false
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//    }
//
//    func sortByDate() {
//        animateOut()
//        sectionObjcs.removeAll()
//        DispatchQueue.main.async {
//            self.sortByDay = true
//            let crtDay = Date()
//            let today: String = HelperClass.dateFormatterShort1.string(from: crtDay)
//            self.sectionObjcs.append(Section(today))
//
//            guard let tasks = self.fetchedResultsController.fetchedObjects else { print("No task to sort")
//                return
//            }
//            print(tasks.count)
//            for task in tasks {
//                let dateString = HelperClass.dateFormatterShort1.string(from: task.createdAt!)
//                if self.sectionObjcs.last?.name == dateString {
//                    self.sectionObjcs.last?.tasks.append(task)
//                }
//                else {
//                    self.sectionObjcs.append(Section(dateString))
//                    self.sectionObjcs.last?.tasks.append(task)
//                }
//            }
//            if self.sectionObjcs.count > 0 {
//                self.sectionObjcs.first!.name = "To Day"
//            }
//            self.tableView.reloadData()
//        }
//    }
//}
