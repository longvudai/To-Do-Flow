
//
//  CellHidableProtocol.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//
import Foundation
import UIKit

protocol CellHidable {
  //  func getIndexPaths(from indexPath: IndexPath) -> [IndexPath]
    func showItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath)
    func hideItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath)
}
