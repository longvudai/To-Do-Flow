//
//  ViewModelPressible.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit


protocol ViewModelPressiable {
    func cellPressed(_ tableView: UITableView, atIndexPath indexPath: IndexPath, withViewModel viewModel: SectionRepresentable)
}
