
//
//  RemindAtLocationViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

class RemindAtLocationViewModel {
    let task: Task
    
    var toggleLocationReminderStatus: Bool {
        return task.hasLocationReminder
    }
    
    init(task: Task) {
        self.task = task
    }
}

extension RemindAtLocationViewModel: SectionRepresentable {
    var numberOfRows: Int {
        var numberOfRows = 1
        
        if toggleLocationReminderStatus {
            numberOfRows = 2
        }
        return numberOfRows
    }
    
    var titleForHeader: String? {
        return nil
    }
    
    func cellIdentifier(for index: Int) -> String {
        var identifier: String
        
        if index == 0 {
            identifier = DetailRemindMeOnLocationCell.identifier
        }
        else {
            identifier = DetailLocationPickerCell.identifier
        }
        return identifier
    }
    
    func viewModel(for index: Int) -> RowViewModel {
        if index == 0 {
            return DetailRemindAtLocationCellViewModel(task: task)
        }
        else {
            return DetailLocationPickerCellViewModel(task: task)
        }
    }
    
}
