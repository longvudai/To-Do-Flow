//
//  NotesViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

struct NotesViewModel {
    let task: Task
    
    var text: String {
        var text: String = ""
        
        if let content = task.content { text = content }
        return text
    }
}


extension NotesViewModel: SectionRepresentable {
    var numberOfRows: Int {
        return 1
    }
    
    var titleForHeader: String? {
        return "Notes"
    }
    
    func cellIdentifier(for index: Int) -> String {
        return DetailNotesCell.identifier
    }
    
    func viewModel(for index: Int) -> RowViewModel {
        return DetailNotesCellViewModel(task: task)
    }
}
