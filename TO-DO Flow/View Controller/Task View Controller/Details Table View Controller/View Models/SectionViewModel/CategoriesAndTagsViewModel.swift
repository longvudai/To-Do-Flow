//
//  CategoriesAndTagsViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

class CategoriesAndTagsViewModel {
    let task: Task
    
    init(task: Task) {
        self.task = task
        
    }

    
    private let numberOfRowsWithTagsPicker = 3     // Number of rows when tags picker is shown
    private let numberOfRowsWithoutTagsPicker = 2   // Number of rows when tags picker is hidden
    
    var cellIndexPath: IndexPath? = nil
    
    // Handle Date Picker
    var hasInlineCell: Bool {
        return self.cellIndexPath != nil
    }
    
    // Determines if the given indexPath points to a cell that contains UIDatePicker
    func rowHasCell(for index: Int) -> Bool {
        return self.hasInlineCell && self.cellIndexPath?.row == index
    }
    
}

extension CategoriesAndTagsViewModel: SectionRepresentable {
    var numberOfRows: Int {
        return hasInlineCell ? numberOfRowsWithTagsPicker : numberOfRowsWithoutTagsPicker
    }
    
    var titleForHeader: String? {
        return nil
    }
    
    func cellIdentifier(for index: Int) -> String {
        var identifier: String
        
        if index == 0 {
            identifier = DetailCategoriesCell.identifier
        }
        else if index == 1 {
            identifier = DetailTagsCell.identifier
        }
        else {
            identifier = DetailTagsPickerCell.identifier
        }
        return identifier
    }
    
    func viewModel(for index: Int) -> RowViewModel {
        if index == 0 {
            return DetailCategoiesCellViewModel(task: task)
        }
        else if index == 1 {
            return DetailTagsCellViewModel(task: task)
        }
        else {
            return DetailTagsPickerViewModel(task: task)
        }
        
    }
}
