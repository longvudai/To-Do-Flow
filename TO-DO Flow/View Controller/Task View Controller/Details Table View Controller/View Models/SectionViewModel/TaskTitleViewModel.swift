//
//  TaskTitleViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

struct TaskTitleViewModel {
    let task: Task
}


extension TaskTitleViewModel: SectionRepresentable {
    var numberOfRows: Int {
        return 1
    }
    
    var titleForHeader: String? {
        return "Title"
    }
    
    func cellIdentifier(for index: Int) -> String {
        return DetailTitleCell.identifier
    }
    
    func viewModel(for index: Int) -> RowViewModel {
        return DetailTitleCellViewModel(task: task)
    }
}
