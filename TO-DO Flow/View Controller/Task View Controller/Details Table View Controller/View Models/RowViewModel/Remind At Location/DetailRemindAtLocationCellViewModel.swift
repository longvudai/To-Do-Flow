//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class DetailRemindAtLocationCellViewModel {
    
    let task: Task
    init(task: Task) {
        self.task = task
    }
    
    var isOn: Bool {
        return task.hasLocationReminder
    }
}

extension DetailRemindAtLocationCellViewModel: RowViewModel {
    
}

extension DetailRemindAtLocationCellViewModel: CellHidable {
    func showItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section)]
        tableView.insertRows(at: indexPaths, with: .fade)
    }
    
    func hideItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section)]
        tableView.deleteRows(at: indexPaths, with: .fade)
    }
}

extension DetailRemindAtLocationCellViewModel: ViewModelPressiable {
    func cellPressed(_ tableView: UITableView, atIndexPath indexPath: IndexPath, withViewModel viewModel: SectionRepresentable) {
        
        guard let viewModel = viewModel as? RemindAtLocationViewModel else { return }
        
        tableView.beginUpdates()
        
        if viewModel.toggleLocationReminderStatus {
            //            self.displayedDate = Date()
            //            let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 1))
            //            cell?.detailTextLabel?.text = HelperClass.dateFormatterShort.string(from: self.displayedDate)
            showItemAtIdxPath(tableView, at: indexPath)
        }
        else {
            hideItemAtIdxPath(tableView, at: indexPath)
        }
        
        tableView.endUpdates()
    }
}

