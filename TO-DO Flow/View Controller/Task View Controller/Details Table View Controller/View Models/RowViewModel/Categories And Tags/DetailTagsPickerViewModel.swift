//
//  DetailTagsPickerViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DetailTagsPickerViewModel {
    let task: Task
    
    init(task: Task) {
        self.task = task
        fetchTag()
    }
    
    var allTag: [Tag] = []
    
    private func fetchTag() {
        let fetchRequest: NSFetchRequest<Tag> = Tag.fetchRequest()
        
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Tag.colorName), ascending: true)]
        
        // Perform Fetch Request
        task.managedObjectContext?.performAndWait {
            do {
                let tags = try fetchRequest.execute()
                
                allTag.removeAll()
                self.allTag = tags
            }
            catch {
                let fetchError = error
                print("Unable to excute Fetch Request")
                print("\(fetchError), \(fetchError.localizedDescription)")
            }
        }
    }
        
}

extension DetailTagsPickerViewModel: RowViewModel {
    
}

extension DetailTagsPickerViewModel: HorizontalScrollerViewDataSource {
    func numberOfViews(in horizontalScrollerView: HorizontalScrollerView) -> Int {
        return self.allTag.count
    }
    
    func horizontalScrollerView(_ horizontalScrollerView: HorizontalScrollerView, viewAt index: Int) -> UIView {
            let tags = self.allTag
            let tag = tags[index]
            let tagView = TagView(frame: CGRect(x: 0, y: 0, width: 30, height: 30), tagColor: tag.colorName!)
    
            //let indexPath = IndexPath(row: index, section: 0)
    
            isContained(tag: tag) ? tagView.highlightTag(true) : tagView.highlightTag(false)
    
            return tagView
    }
    
    private func isContained(tag: Tag) -> Bool {
        if let containsTag = task.tag?.contains(tag), containsTag == true {
            return true
        }
        else {
            return false
        }
    }
    
    func didSelectTag(withTagView tagView: TagView, at index: Int) {
        let tags = allTag
        let tag = tags[index]

        if isContained(tag: tag) {
            task.removeFromTag(tag)
            tagView.highlightTag(false)
        }
        else {
            task.addToTag(tag)
            tagView.highlightTag(true)
        }
    }
}
