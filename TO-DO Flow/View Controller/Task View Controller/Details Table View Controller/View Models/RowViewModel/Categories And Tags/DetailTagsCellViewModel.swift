//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

struct DetailTagsCellViewModel {
    
    let task: Task
    
    var text: String {
        var text: String = ""
        if let title = task.title {
            text = title
        }
        return text
    }
}

extension DetailTagsCellViewModel: RowViewModel {
}

extension DetailTagsCellViewModel: CellHidable {
    func showItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row + 1 ,
                                    section: indexPath.section)]
        tableView.insertRows(at: indexPaths, with: .fade)
    }
    
    func hideItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row + 1 ,
                                    section: indexPath.section)]
        tableView.deleteRows(at: indexPaths, with: .fade)
    }
}

extension DetailTagsCellViewModel: ViewModelPressiable & CellSelectable {
    func cellPressed(_ tableView: UITableView, atIndexPath indexPath: IndexPath, withViewModel viewModel: SectionRepresentable) {
        
        guard let viewModel = viewModel as? CategoriesAndTagsViewModel else { return }
        
        tableView.beginUpdates()
        
        // Show the picker if date cell was selected and picker is not shown
        if viewModel.hasInlineCell {
            self.hideItemAtIdxPath(tableView, at: indexPath)
            viewModel.cellIndexPath = nil
        }
        // Hide the picker if date cell was selected and picker is shown
        else {
            self.showItemAtIdxPath(tableView, at: indexPath)
            viewModel.cellIndexPath = IndexPath(row: indexPath.row+1, section: 3)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.endUpdates()
    }
}
