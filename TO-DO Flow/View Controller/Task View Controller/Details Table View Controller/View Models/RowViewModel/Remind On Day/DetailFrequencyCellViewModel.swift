//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

struct DetailFrequencyCellViewModel {
    
    let task: Task
    
    var text: String {
        var text: String = ""
        
        if let title = task.title {
            text = title
        }
        
        return text
    }
}

extension DetailFrequencyCellViewModel: RowViewModel {
    
}
