//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class DetailAlarmCellViewModel {
    
    let task: Task
    
    init(task: Task, displayDate: Date) {
        self.task = task
        self.displayDate = displayDate
    }
    
    var text: String = "Alarm"
    
    var displayDate: Date
    
    var date: String {
        return HelperClass.dateFormatterShort.string(from: displayDate)
    }
}

extension DetailAlarmCellViewModel: RowViewModel {
    
}

extension DetailAlarmCellViewModel: CellHidable {
    func showItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section)]
        tableView.insertRows(at: indexPaths, with: .fade)
    }
    
    func hideItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section)]
        tableView.deleteRows(at: indexPaths, with: .fade)
    }
    

}

extension DetailAlarmCellViewModel: ViewModelPressiable & CellSelectable {
    func cellPressed(_ tableView: UITableView, atIndexPath indexPath: IndexPath, withViewModel viewModel: SectionRepresentable) {
        guard let viewModel = viewModel as? RemindOnDateViewModel else { return }
        
        tableView.beginUpdates()
        
        if viewModel.hasInlineCell {
            
            hideItemAtIdxPath(tableView, at: indexPath)
            
            viewModel.cellIndexPath = nil
            
            let cell = tableView.cellForRow(at: indexPath)
            if let cell = cell as? CellConfigurable {
                cell.configure(withViewModel: self)
            }
            
            // Hide the picker if date cell was selected and picker is shown
        } else {
            showItemAtIdxPath(tableView, at: indexPath)
            viewModel.cellIndexPath = IndexPath(row: indexPath.row+1, section: indexPath.section)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.endUpdates()
    
        updateDatePicker(tableView, withViewModel: viewModel)
    }
    
    // Update the UIDatePicker's value to match with the date of the cell above it
    private func updateDatePicker(_ tableView: UITableView, withViewModel viewModel: RemindOnDateViewModel) {
        
        if let idxPath = viewModel.cellIndexPath {
            if let datePickerCell = tableView.cellForRow(at: idxPath) as? DetailDatePickerCell {
                datePickerCell.datePicker.date = displayDate
            }
        }
    }
}
