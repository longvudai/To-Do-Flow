//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class DetailRemidOnDateCellViewModel {
    
    let task: Task
    
    var isOn: Bool {
        return task.hasAlarm
    }
    init(task: Task) {
        self.task = task
    }

}

extension DetailRemidOnDateCellViewModel: RowViewModel {
    
}


extension DetailRemidOnDateCellViewModel: CellHidable {
    func showItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
        let indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section),
                          IndexPath(row: indexPath.row+2, section: indexPath.section)]
        tableView.insertRows(at: indexPaths, with: .fade)
    }
    
    func hideItemAtIdxPath(_ tableView: UITableView, at indexPath: IndexPath) {
//        var indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section),
//                          IndexPath(row: indexPath.row+2, section: indexPath.section)]
//
//        if self.hasInlineCell {
//            self.cellIndexPath = nil
//            indexPaths.append(IndexPath(row: indexPath.row+3, section: indexPath.section))
//        }
//
//        tableView.deleteRows(at: indexPaths, with: .fade)
    }
}

extension DetailRemidOnDateCellViewModel: ViewModelPressiable {
    func cellPressed(_ tableView: UITableView, atIndexPath indexPath: IndexPath, withViewModel viewModel: SectionRepresentable) {

        // Check Permission Acess Reminder
        
        tableView.beginUpdates()
        
        
        guard let viewModel = viewModel as? RemindOnDateViewModel else {return}
        
        if viewModel.toggleReminderStatus {
            showItemAtIdxPath(tableView, at: indexPath)
        }
        else {
            var indexPaths = [IndexPath(row: indexPath.row+1, section: indexPath.section),
                              IndexPath(row: indexPath.row+2, section: indexPath.section)]
            
            if viewModel.hasInlineCell {
                viewModel.cellIndexPath = nil
                indexPaths.append(IndexPath(row: indexPath.row+3, section: indexPath.section))
            }
            
            tableView.deleteRows(at: indexPaths, with: .fade)
          //  hideItemAtIdxPath(tableView, at: indexPath)
        }
        
        tableView.endUpdates()
    }
}



