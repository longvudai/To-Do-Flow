//
//  DetailTitleCellViewModel.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation

struct DetailNotesCellViewModel {
    
    let task: Task
    
    var text: String {
        get {
            var text: String = "Add some notes..."
            if let content = task.content { text = content }
            return text
        }
        set {
            print("in notes")
            task.content = newValue
        }
    }
}

extension DetailNotesCellViewModel: RowViewModel {}

extension DetailNotesCellViewModel: TextViewRepresentable {}
