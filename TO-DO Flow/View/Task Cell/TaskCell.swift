//
//  TaskCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 7/21/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol TaskCellDelegate: class {
    func swipwRight(_ TaskCell: TaskCell)
    func showDetail(_ sender: UIButton)
}

class TaskCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var taskTitleTextView: UITextView!
    @IBOutlet weak var taskTagsView: UIView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var dateAndFrequency: UILabel!
    
    @IBOutlet weak var widthOfTags: NSLayoutConstraint!
    @IBOutlet weak var heightTaskCellConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTaskTagExpanded: NSLayoutConstraint!
    
    @IBOutlet weak var dateAndLabelHeightConstraint: NSLayoutConstraint!
    // MARK: - Properties
    
    var isFinished: Bool! {
        didSet {
            if isFinished {
                self.taskTitleTextView.strikeThrough(isFinished)
                taskTitleTextView.isEditable = false
                taskTitleTextView.isSelectable = false
            }
            else {
                self.taskTitleTextView.strikeThrough(isFinished)
                taskTitleTextView.isEditable = true
                taskTitleTextView.isSelectable = true
            }
        }
    }
    
    var delegate: TaskCellDelegate?
    // MARK: - Reusable Identifier

    
    // MARK: - Action
    
    @IBAction func infoWasPressed(_ sender: UIButton) {
        delegate?.showDetail(sender)
    }
    
    
    func setupSwipe() {
        var swipeGesture = UISwipeGestureRecognizer()
        
        let directions: [UISwipeGestureRecognizerDirection] = [.right]
        for direction in directions {
            swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRightView(_:)))
            taskTitleTextView.addGestureRecognizer(swipeGesture)
            swipeGesture.direction = direction
            taskTitleTextView.isUserInteractionEnabled = true
            taskTitleTextView.isMultipleTouchEnabled = true
        }
    }
    
    override func prepareForReuse() {
        taskTitleTextView.text = nil
        taskTitleTextView.strikeThrough(false)
    }
    
    @objc private func swipeRightView(_ sender : UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 1.0) {
            if sender.direction == .right {
                self.delegate?.swipwRight(self)
            }
          //  self.taskTitleTextView.layoutIfNeeded()
          //  self.taskTitleTextView.setNeedsDisplay()
        }
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        selectionStyle = .none
        
        // Configure Text View
        taskTitleTextView.isScrollEnabled = false
        taskTitleTextView.backgroundColor = .white
        
        // Info Button
        infoButton.isHidden = true
        
        // Configure Task Tags View
        taskTagsView.backgroundColor = .white
        taskTagsView.clipsToBounds = true
    }
}

extension TaskCell {
    func configure(withViewModel viewModel: TaskViewDataViewModel) { // edit
        taskTitleTextView.text = viewModel.title
        taskTagsView.addSubview(viewModel.taskTagsView)
        taskTagsView.frame = CGRect(x: taskTagsView.frame.minX, y: taskTagsView.frame.minY, width: viewModel.taskTagsView.frame.width, height: viewModel.taskTagsView.frame.height)
        
        
        isFinished = viewModel.isFinished
        
        dateAndFrequency.text = viewModel.dateAndFrequency
        
        //  Configure Date and Frequency
        if viewModel.dateAndFrequency != nil {
            dateAndLabelHeightConstraint.priority = UILayoutPriority(rawValue: 250)
        }
        else {
            dateAndLabelHeightConstraint.priority = UILayoutPriority(rawValue: 999)
        }
    }
    
}
