//
//  DetailRemindMeOnLocationCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol DetailRemindAtLocationCellDelegate: class {
    func switchButtonValueChanged(_ sender: UISwitch)
}

class DetailRemindMeOnLocationCell: UITableViewCell {

    @IBOutlet weak var switchButton: UISwitch!
    
    weak var delegate: DetailRemindAtLocationCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
    @IBAction func switchValueChanged(_ sender: Any) {
        delegate?.switchButtonValueChanged(sender as! UISwitch)
    }
    
    
}
extension DetailRemindMeOnLocationCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailRemindAtLocationCellViewModel else { fatalError("Unexpected type") }
    
        switchButton.isOn = viewModel.isOn
        
    }
    
    func delegate(_ viewController: UIViewController) {
        if let viewVC = viewController as? DetailsTableVC {
            
            self.delegate = viewVC
        }
    }
}

