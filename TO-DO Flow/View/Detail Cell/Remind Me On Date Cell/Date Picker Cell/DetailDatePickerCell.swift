//
//  DetailDatePickerCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol DatePickerDelegate: class {
    func datePickerValueChanged(_ sender: UIDatePicker)
}

class DetailDatePickerCell: UITableViewCell {

    @IBOutlet weak var datePicker: UIDatePicker!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    weak var delegate: DatePickerDelegate?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }

    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        delegate?.datePickerValueChanged(sender)
        //        if self.hasInlineDatePicker {
        //            let dateCellIndexPath = IndexPath(row: self.datePickerIndexPath!.row-1, section: 1)
        //
        //            let cell = self.tableView.cellForRow(at: dateCellIndexPath)
        //            // Update the displayed date
        //            cell?.textLabel?.text = HelperClass.dateFormatterFull.string(from: datePicker.date)
        //            cell?.textLabel?.textColor = .blue
        //            cell?.detailTextLabel?.text = ""
        
        //            self.displayedDate = datePicker.date
        //        }
    }
}

extension DetailDatePickerCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailDatePickerCellViewModel else { fatalError("Unexpected type") }
        datePicker.date = viewModel.displayDate
    }
    
    func delegate(_ viewController: UIViewController) {
        if let viewVC = viewController as? DetailsTableVC {
            delegate = viewVC
        }
    }
}
