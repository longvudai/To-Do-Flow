//
//  DetailAlarmCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class DetailAlarmCell: UITableViewCell {

    @IBOutlet weak var alarmLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
}

extension DetailAlarmCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailAlarmCellViewModel else { fatalError("Unexpected type") }
        
        alarmLabel.text = viewModel.text
        dateLabel.text = viewModel.date
    }
    
    func delegate(_ viewController: UIViewController) {
        
    }
}
