//
//  DetailCategoriesCell.swift
//  TO-DO Flow
//
//  Created by Dai Long on 8/23/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class DetailCategoriesCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: nil)
    }
}

extension DetailCategoriesCell: CellConfigurable {
    func configure(withViewModel viewModel: RowViewModel) {
        guard let viewModel = viewModel as? DetailCategoiesCellViewModel else { fatalError("Unexpected type") }
        
        categoryLabel.text = "No cate"
    }
    func delegate(_ viewController: UIViewController) {
        
    }
}

