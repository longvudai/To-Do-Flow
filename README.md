# To-Do Flow Application
The simple Reminders app that comes with iPhones might be the easiest way to keep up with things you have to do. You can add tasks, sort them into lists.

Want to do more? There's a lot of power hidden behind the i button on each task. You can add a due date to tasks or have it notify you about tasks at specific locations so you won't forget to pick up stuff at the store, say. Enable the calendar from the View menu and you can drag tasks to their due date. There's even a note field, a priority setting to organize tasks, and the option to share task lists with other Reminders users.

# Demo

Lock Screen            |  Main
:-------------------------:|:-------------------------:
![Alt Text](/Demo/UNADJUSTEDNONRAW_thumb_1e6.jpg "Screen 1")  |  ![Alt Text](/Demo/UNADJUSTEDNONRAW_thumb_1e8.jpg "Screen 2")

Details            |    Add task
:-------------------------:|:-------------------------:
![Alt Text](/Demo/UNADJUSTEDNONRAW_thumb_1e9.jpg "Screen 3") | ![Alt Text](/Demo/IMG_0122.png "Screen 4")


[Video](https://gitlab.com/dailong/To-Do-Flow/blob/master/ScreenRecording_08-07-2018%2011.MP4)
